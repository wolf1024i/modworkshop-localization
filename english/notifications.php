<?php
$l['myalerts_pm'] = '{1} sent you a new private message titled <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} quoted you in <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} replied to your thread <b>"{2}"</b>. There may be more posts after this.';
$l['myalerts_subscribed_thread'] = '{1} replied to your subscribed thread <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} rated your thread <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} voted in your poll in <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} has updated the followed mod {2}";
$l['alert_subbed_discussion_modpage'] = "{1} posted a comment in the mod {2} which you are subscribed to";
$l['no_alerts_found'] = "No notifications found";
$l['no_more_alerts_found'] = "Couldn't get more notifications";
$l['notifications'] = 'Notifications';
$l['browse_all_notifications'] = 'Browse All Notifications';
