<?php

$l['mydownloads_being_updated'] = 'Aktualisiert';
$l['mydownloads_meta_by'] = '{1} by {2}'; //Mod X by Y; //???
$l['mydownloads_cannot_rate_own'] = 'Sie können ihre eigenen Mods nicht bewerten.';
$l['mydownloads_collaborators_desc'] = 'Koautoren sind Benutzer, denen Sie die Erlaubnis gegeben haben, die Mod zu bearbeiten. Sie können weder die Mod löschen, Koautoren bearbeiten oder den Beistz der Mod übertragen';
$l['mydownloads_comment_banned'] = "Sie können nicht kommentieren, während Sie gebannt sind.";
$l['mydownloads_delete_confirm'] = "Sind Sie sicher, dass Sie diesen Kommentar löschen wollen?";
$l['mydownloads_download_description'] = "Beschreibung";
$l['mydownloads_download_is_suspended'] = '
Diese Mod wurde suspendiert und ist nur für den Author und Seitenmitarbeiter sichtbar.
<br/>Die Suspendierung ist entweder temporär für Untersuchungen oder permanent aufgrund eines Bruches der <a style="text-decoration:underline;" href="/rules">Regeln</a>.
<br/>Sollten Sie die Seitenmitarbeiter deswegen kontaktieren wollen oder wenn ihre Mod aktualisiert wurde, um Regelkonform zu sein, schreiben Sie bitte eine \"Bewerbung für Beendigung\" <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">hier</a>.';
$l['mydownloads_download'] = "Herunterladen";
$l['mydownloads_license'] = 'Lizens';
$l['mydownloads_report_download'] = 'Mod melden';
$l['show_download_link_warn'] = 'Seien Sie vorsichtig bei verdächtigen Links. Wenn Sie denken der Link ist bösartig, bitte melden Sie die Mod';
$l['show_download_link'] = 'Zeige Download Link';
$l['show_files'] = 'Zeige Dateien';
$l['submitted_by'] = 'Eingereicht von';
$l['subscribe_commnet_help'] = 'Bekomme Benachrichtigung, wenn jemand in diesem Kommentarbereich kommentiert';
$l['follow_mod_help'] = 'Verfolge diese Mod, um sie in \"Verfolge Mods\" anzuzeigen und Benachrichtigung erhalten, wenn die Mod aktualisiert wurde';
$l['mydownloads_download_comments'] = "Kommentare";
$l['mydownloads_unsuspend_it'] = 'Unsuspendieren';
$l['mydownloads_suspend_it'] = 'Suspendieren';
$l['mydownloads_files_alert'] = '<strong>KEINE DATEIEN</strong> | Da die Mod keine Dateien hat, ist sie unsichtbar.';
$l['mydownloads_files_alert_waiting'] = '<strong>KEINE GENEHMIGTEN DATEIEN</strong> | Die Mod ist unsichtbar bis die Dateien genehmigt werden.';
$l['share'] = 'Teilen';
?>