<?php
$l['myalerts_pm'] = '{1} wysłał ci wiadomośc prywatną pod tytułem <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} zacytował cię w <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} odpowiedział na twój wątek <b>"{2}"</b>. Po tym może być więcej postów.';
$l['myalerts_subscribed_thread'] = '{1} odpowiedział na zasubskrybowany wątek <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} ocenił twój wątek <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} zagłosował w ankiecie z <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} aktualizował obserwowany mod {2}";
$l['alert_subbed_discussion_modpage'] = "{1} dodał komentarz do modyfikacji {2}, którą zasubskrybowałeś.";
$l['no_alerts_found'] = "Nie znaleziono żadnych powiadomień.";
$l['no_more_alerts_found'] = "Nie znaleziono więcej powiadomień.";
$l['notifications'] = 'Powiadomienia';
$l['browse_all_notifications'] = 'Przeglądaj wszystkie powiadomienia';
