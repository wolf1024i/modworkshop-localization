<?php
$l['restore'] = "Przywróć";
$l['move_to_trash'] = "Przenieś do śmietnika";
$l['compose'] = "Napisz wiadomość";
$l['trash'] = "Śmietnik";
$l['inbox'] = "Odebrane";
$l['sent_messages'] = "Wysłano wiadomość";
$l['receiver'] = "Odbiorca";
$l['sender'] = "Nadawca";
$l['actions'] = "Akcje";
$l['subject'] = "Tytuł";
$l['message'] = "Wiadomość";
$l['to'] = "Do";
$l['BCC'] = "UDW";
$l['no_messages_found'] = "Nie znaleziono wiadomości";
$l['no_more_messages_found'] = "Nie znaleziono więcej wiadomości.";
$l['messages'] = "Wiadomości";
$l['send_message_banned'] = "Nie możesz wysłać wiadomości kiedy jesteś zbanowany.";
