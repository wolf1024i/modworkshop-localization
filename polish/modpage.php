<?php

$l['mydownloads_being_updated'] = 'Aktualizowanie';
$l['mydownloads_meta_by'] = '{1} przez {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = 'Nie możesz oceniać własnych modyfikacji.';
$l['mydownloads_collaborators_desc'] = 'Współautorzy to osoby które mogą zmieniać stronę modyfikacji. Nie mogą usunąć modyfikacji ani dodawać współałtorów.';
$l['mydownloads_comment_banned'] = "Nie możesz dodawać komentarzy kiedy jesteś zbanowany.";
$l['mydownloads_delete_confirm'] = "Czy na pewno chcesz usunąć ten komentarz?";
$l['mydownloads_download_description'] = "Opis";
$l['mydownloads_download_is_suspended'] = 'Ta modyfikacja jest zawieszona i jest widoczna TYLKO dla Twórcy modyfikacji i administracji.
<br/>Zawieszenie może być tymczasowe w celu zbadania modyfikacji lub permanentne za złamanie <a style="text-decoration:underline;" href="/rules">zasad</a>.
<br/>Jeżeli chcesz porozmawiać z administracją na temat tego zawieszenia, lub jeżeli zaktualizowałeś mod w zgodności z zasadami, możesz napisać wniosek o "Przywrócenie Modyfikacji" <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">tutaj</a>.';
$l['mydownloads_download_changelog'] = 'Lista Zmian';
$l['mydownloads_download'] = "Pobierz";
$l['mydownloads_license'] = 'Licencja';
$l['mydownloads_report_download'] = 'Zgłoś Modyfikację';
$l['show_download_link_warn'] = 'Uważaj na podejrzane linki. Jeśli obawiasz się, że link prowadzi do wirusa, możesz zgłosić tę modyfikację.';
$l['show_download_link'] = 'Pokaż link do pobrania';
$l['show_files'] = 'Pokaż pliki';
$l['submitted_by'] = 'Przesłane przez';
$l['subscribe_commnet_help'] = 'Dostań powiadomienie gdy ktoś napisze komentarz.';
$l['follow_mod_help'] = 'Obserwuj modyfikację, żeby dostawać powiadomienia gdy mod zostanie aktualizowany.';
$l['mydownloads_download_comments'] = "Komentarze";
$l['mydownloads_unsuspend_it'] = 'Przywróć';
$l['mydownloads_suspend_it'] = 'Zawieś';
$l['mydownloads_files_alert'] = '<strong>BRAK PLIKÓW</strong> | Ze względu na brak plików, modyfikacja jest niewidoczna.';
$l['mydownloads_files_alert_waiting'] = '<strong>BRAK ZATWIERDZONYCH PLIKÓW</strong> | Ze względu na brak plików zatwierdzonych przez administrację, modyfikacja jest niewidoczna.';
$l['share'] = 'Udostępnij';
