<?php

$l['mydownloads_being_updated'] = '上传（更新）您的MOD';
$l['mydownloads_meta_by'] = '{1} 作者是: {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = '您不能赞自己的MOD！';
$l['mydownloads_collaborators_desc'] = '“联合创作者”有编辑您MOD的权限，但无法删除MOD，也无法向他人转让您MOD的所有权。';
$l['mydownloads_comment_banned'] = "您被封禁了！无法评论！快快去管理员大大那里问一下吧！";
$l['mydownloads_delete_confirm'] = "您确定要删除此评论吗？";
$l['mydownloads_download_description'] = "简介";
$l['mydownloads_download_is_suspended'] = '
本MOD暂不可用，只有其作者和管理员可见
<br/>该MOD正在重新审核，因为疑似违反了 <a style="text-decoration:underline;" href="/rules">rules</a>.
<br/>如果您需要与管理人员联系或您的MOD已经修改至符合规则，请发送解禁申请，我们会尽快审核您的MOD／<a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">点这噢~^o^~</a>.';
$l['mydownloads_download_changelog'] = '更新记录';
$l['mydownloads_download'] = "下载";
$l['mydownloads_license'] = '开源许可';
$l['mydownloads_report_download'] = '举报';
$l['show_download_link_warn'] = '请注意可疑的下载链接。如果您认为该链接为恶意链接，请举报该mod！';
$l['show_download_link'] = '展开下载链接';
$l['show_files'] = '展开文件';
$l['submitted_by'] = '上传者：';
$l['subscribe_commnet_help'] = '当有人在评论区评论时提醒';
$l['follow_mod_help'] = '订阅该MOD并且在您关注的MOD中显示它，当它有更新时我们会通知您！';
$l['mydownloads_download_comments'] = "评论";
$l['mydownloads_unsuspend_it'] = '解除下载限制';
$l['mydownloads_suspend_it'] = '限制下载';
$l['mydownloads_files_alert'] = '<strong>没有上传文件！</strong> | 该MOD没有文件，所以当前MOD已被隐藏。';
$l['mydownloads_files_alert_waiting'] = '<strong>审核中</strong> | 我们将会尽快审核您的大作，在通过之后开放下载，请稍安毋躁(＾▽＾)';
$l['share'] = '分享该MOD';