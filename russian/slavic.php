<?php

//This is only for slavic languages at the moment as their spelling changes in certain cases requiring us to add an additional "plural".

$l['year2'] = 'years'; 
$l['months2'] = 'months'; 
$l['days2'] = 'days';
$l['weeks2'] = 'weeks';
$l['hours2'] = 'hours';
$l['minutes2'] = 'minutes';
$l['seconds2'] = 'seconds';
