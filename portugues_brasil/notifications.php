<?php
$l['myalerts_pm'] = '{1} te enviou uma mensagem com assunto <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} te mencionou em <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} respondeu a sua discussão <b>"{2}"</b>. Pode ter mais posts depois dessa.';
$l['myalerts_subscribed_thread'] = '{1} respondeu a sua discussão inscrito <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} avaliou a sua discussão <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} votou na sua votação <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} atualizou o mod {2}";
$l['alert_subbed_discussion_modpage'] = "{1} postou um comentário no mod {2} que você se inscreveu";
$l['no_alerts_found'] = "Sem notificações achadas";
$l['no_more_alerts_found'] = "Não tem mais notificações";
$l['notifications'] = 'Notificações';
$l['browse_all_notifications'] = 'Navegar em todas as notificações';
